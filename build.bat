@echo on

dir
set PYTHON=C:\Python27-x64\python.exe
set SIP=sip-4.18.1
set CURL=curl -fsSL -o 
set UNZIP=7z x -bd -y

%CURL% get-pip.py https://bootstrap.pypa.io/get-pip.py
%PYTHON% get-pip.py
C:\Python27-x64\Scripts\pip install wheel

%CURL% %SIP%.zip "https://sourceforge.net/projects/pyqt/files/sip/%SIP%/%SIP%.zip"
%CURL% qt.exe http://qt-mirror.dannhauer.de/archive/qt/5.7/5.7.0/qt-opensource-windows-x86-msvc2015_64-5.7.0.exe
qt.exe -h
qt.exe /?
%UNZIP% %SIP%.zip

cd %SIP%
%PYTHON% configure.py -h
nmake Makefile
REM %PYTHON% configure.py --use-qmake -p win32-msvc2015
type makefile
dir